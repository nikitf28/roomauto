/**
   BasicHTTPSClient.ino

    Created on: 20.08.2018

*/

#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

#include <ESP8266HTTPClient.h>

#include <WiFiClientSecureBearSSL.h>

#include <ArduinoJson.h>

// Fingerprint for demo URL, expires on June 2, 2021, needs to be updated well before this date
String token = "bb33ea39caba318c14fba98bc61ae6a11d5cdfb5db2dff3d8941235f1ee28f60a2bb50c0d23cde2d059c0";
String group_id = "202511572";

String key, server, ts;

String buttons = "{\"one_time\":false,\"buttons\":[[{\"action\":{\"type\":\"text\",\"label\":\"Открыть дверь 🚪\"},\"color\":\"primary\"}]]}";

ESP8266WiFiMulti WiFiMulti;

bool inf;

void sendMSG(String peer_id, String msg, String keyboard){
    std::unique_ptr<BearSSL::WiFiClientSecure>client(new BearSSL::WiFiClientSecure);
    client->setInsecure();
    HTTPClient https;
    String URL = "https://api.vk.com/method/messages.send?random_id=0&peer_id=" + peer_id + "&v=5.130&access_token=" + token + "&message=" + msg + "&keyboard=" + keyboard;
    Serial.print(URL);
    if (https.begin(*client, URL)) {  // HTTPS
      Serial.print("[HTTPS] GET...\n");
      // start connection and send HTTP header
      int httpCode = https.GET();

      // httpCode will be negative on error
      if (httpCode > 0) {
        // HTTP header has been send and Server response header has been handled
        Serial.printf("[HTTPS] GET... code: %d\n", httpCode);

        // file found at server
        if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
          String payload = https.getString();
          Serial.println(payload);
        }
      } else {
        Serial.printf("[HTTPS] GET... failed, error: %s\n", https.errorToString(httpCode).c_str());
        String payload = https.getString();
        Serial.println(payload);
      }

      https.end();
    } else {
      Serial.printf("[HTTPS] Unable to connect\n");
    }
}

void prepareVK(){
    std::unique_ptr<BearSSL::WiFiClientSecure>client(new BearSSL::WiFiClientSecure);
    client->setInsecure();
    HTTPClient https;
    String URL = "https://api.vk.com/method/groups.getLongPollServer?group_id=" + group_id + "&v=5.130&access_token=" + token;
    Serial.print(URL);
    if (https.begin(*client, URL)) {  // HTTPS
      Serial.print("[HTTPS] GET...\n");
      // start connection and send HTTP header
      int httpCode = https.GET();

      // httpCode will be negative on error
      if (httpCode > 0) {
        // HTTP header has been send and Server response header has been handled
        Serial.printf("[HTTPS] GET... code: %d\n", httpCode);

        // file found at server
        if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
          String payload = https.getString();
          Serial.println(payload);
          
          const size_t capacity = JSON_OBJECT_SIZE(3) + JSON_ARRAY_SIZE(2) + 60;
          DynamicJsonBuffer jsonBuffer(capacity);
          JsonObject& root = jsonBuffer.parseObject(payload);
          
          if (!root.success()) {
             Serial.println(F("Parsing failed!"));
             return;
          }
          key = root["response"]["key"].as<String>();
          server = root["response"]["server"].as<String>();
          ts = root["response"]["ts"].as<String>();
          Serial.println(key);
          Serial.println(server);
          Serial.println(ts);
        }
      } else {
        Serial.printf("[HTTPS] GET... failed, error: %s\n", https.errorToString(httpCode).c_str());
      }

      https.end();
    } else {
      Serial.printf("[HTTPS] Unable to connect\n");
    }
}

void setup() {

  inf = false;

  Serial.begin(115200);
  // Serial.setDebugOutput(true);

  Serial.println();
  Serial.println();
  Serial.println();

  for (uint8_t t = 4; t > 0; t--) {
    Serial.printf("[SETUP] WAIT %d...\n", t);
    Serial.flush();
    delay(1000);
  }

  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP("IT-WIFI", "super618");
}

void loop() {

  // wait for WiFi connection
  if ((WiFiMulti.run() == WL_CONNECTED)) {

    if (!inf){
        prepareVK();
        inf = true;
    }

    std::unique_ptr<BearSSL::WiFiClientSecure>client(new BearSSL::WiFiClientSecure);

    client->setInsecure();

    HTTPClient https;

    https.setTimeout(40000);

    Serial.print("[HTTPS] begin...\n");
    String URL = server + "?act=a_check&key=" + key + "&ts=" + ts + "&wait=25";
    if (https.begin(*client, URL)) {  // HTTPS

      Serial.print("[HTTPS] GET...\n");
      // start connection and send HTTP header
      int httpCode = https.GET();

      // httpCode will be negative on error
      if (httpCode > 0) {
        // HTTP header has been send and Server response header has been handled
        Serial.printf("[HTTPS] GET... code: %d\n", httpCode);

        // file found at server
        if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
          String payload = https.getString();
          Serial.println(payload);

          const size_t capacity = JSON_OBJECT_SIZE(3) + JSON_ARRAY_SIZE(2) + 60;
          DynamicJsonBuffer jsonBuffer(capacity);
          JsonObject& root = jsonBuffer.parseObject(payload);
          ts = root["ts"].as<String>();
          Serial.print(ts);
          if (root.containsKey("failed")){
             int failed = root["failed"].as<int>();
             if (failed == 2 || failed == 3){ 
                inf = false; 
             }
          }
          
          for (int i = 0; i <  root["updates"].size(); i++) {
            Serial.println(root["updates"][i]["type"].as<String>());
            if (root["updates"][i]["type"].as<String>() == "message_new"){
              String msgText = root["updates"][i]["object"]["message"]["text"].as<String>();
              String peer_id = root["updates"][i]["object"]["message"]["peer_id"].as<String>();
              Serial.println(msgText);
              sendMSG(peer_id, "Бла", buttons);
              if (msgText == "Открыть дверь 🚪" || msgText == "Открыть дверь"){
                sendMSG(peer_id, "Открываю 🕐", buttons);
              }
            }
          }
            
        }
      } else {
        Serial.printf("[HTTPS] GET... failed, error: %s\n", https.errorToString(httpCode).c_str());
      }

      https.end();
    } else {
      Serial.printf("[HTTPS] Unable to connect\n");
    }
  }

  Serial.println("Wait 1s before next round...");
  delay(1000);
}
