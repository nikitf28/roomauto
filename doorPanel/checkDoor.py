import requests
import time
from pygame import mixer

mixer.init()
mixer.music.load("sounds/play.mp3")

last = "OPENED"

while True:
    response = requests.get('http://192.168.0.40/check')
    text = response.text

    if last == "CLOSED" and text == "OPENED":
        mixer.music.play()

    last = text
    time.sleep(0.33)
